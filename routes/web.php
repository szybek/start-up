<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('home');})
    ->name('home');

//auth routes
Route::group(['namespace' => 'Auth'], function (\Illuminate\Routing\Router $router) {
	 // Authentication Routes...
    $this->get('profile/login', 'LoginController@showLoginForm')->name('profile.login');
    $this->post('profile/login', 'LoginController@login');
    $this->post('profile/logout', 'LoginController@logout')->name('profile.logout');

    // Registration Routes...
    $this->get('profile/register', 'RegisterController@showRegistrationForm')->name('profile.register');
    $this->post('profile/register', 'RegisterController@register');

    // Password Reset Routes...
    $this->get('profile/password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('profile.password.request');
    $this->post('profile/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('profile.password.email');
    $this->get('profile/password/reset/{token}', 'ResetPasswordController@showResetForm');
    $this->post('profile/password/reset', 'ResetPasswordController@reset');
});

Route::group(['namespace' => 'Profile'], function(\Illuminate\Routing\Router $router) {
    $router->get('profile/show/{user?}', 'ProfileController@showProfile')->where(['user' => '[a-z]+']);
});

Route::group(['namespace' => 'Admin', 'middleware' => ['permission']], function(\Illuminate\Routing\Router $router) {
    $router->get('admin', 'DashboardController@showDashboard')->name('admin.dashboard.index');

    $router->resource('admin/user', 'UserController', [
        'names' => [
            'index'   => 'admin.user.index',
            'show'    => 'admin.user.show',
            'edit'    => 'admin.user.edit',
            'update'  => 'admin.user.update',
            'destroy' => 'admin.user.destroy'
        ],
        'except' => [
            'create',
            'store'
        ]
    ]);

    $router->resource('admin/permission', 'PermissionController', [
        'names' => [
            'index'   => 'admin.permission.index',
            'show'    => 'admin.permission.show', 
            'edit'    => 'admin.permission.edit',
            'update'  => 'admin.permission.update',
        ],
        'except' => [
            'create',
            'store',
            'destroy'
        ]
    ]);
});