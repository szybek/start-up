<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'birth_date'
    ];

    /**
     * Table name `users_details`
     *
     * @var string
     */
    protected $table = 'users_details';

     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
        
    /**
     * Set primary key other than `id`
     *
     * @var string
     */
    protected $primaryKey = 'id_user';

    /**
     * Set  incrementing to false
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Get the user thet owns the company
     */
    public function user()
    {
        $this->belongsTo('App\Models\User', 'id');
    }
}
