<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'describe'
    ];

    protected $table = 'permissions_name';
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get permissions of user name
     */
    public function permissions()
    {
        return $this->belongsToMany('App\Models\UserName', 'permissions', 'id_permission_name', 'id_user_name')
            ->withPivot('value');
    }
}
