<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserName extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'code'
    ];

    protected $table = 'users_name';
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get permissions of user name
     */
    public function permissions()
    {
        return $this->belongsToMany('App\Models\Permission', 'permissions', 'id_user_name', 'id_permission_name')
            ->withPivot('value');
    }
}
