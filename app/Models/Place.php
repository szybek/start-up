<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cord_x', 'cord_y', 'address', 'description', 'id_user'
    ];

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'places';

     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get permissions of user name
     */
    public function meta()
    {
        return $this->belongsToMany('App\Models\PlaceMetaName', 'places_meta', 'id_place', 'id_meta_name')
            ->withPivot('value');
    }
}
