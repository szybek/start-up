<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlaceMetaName extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'places_meta_name';

     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get permissions of user name
     */
    public function place()
    {
        return $this->belongsToMany('App\Models\Place', 'places_meta', 'id_place', 'id_meta_name')
            ->withPivot('value');
    }
}
