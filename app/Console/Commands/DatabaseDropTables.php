<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;

class DatabaseDropTables extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:droptables {--confirm}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Drop all tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $colname = 'Tables_in_' . env('DB_DATABASE');
        $tables  = DB::select('SHOW TABLES');

        foreach ($tables as $table) {
            $droplist[] = $table->$colname;
        }

        $droplist = implode(',' , $droplist);

        DB::beginTransaction();
        DB::statement("DROP TABLE $droplist");
        DB::commit();

        $this->comment("If no errors showed up, all tables were dropped");
    }
}
