<?php

namespace App\Console\Commands;

use DB;

use Illuminate\Console\Command;

class DatabaseDump extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:dump';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Making dump all data from database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $colname = 'Tables_in_' . env('DB_DATABASE');
        $tables  = DB::select('SHOW TABLES');
        $backup;

        if (empty($tables)) {
            $this->comment('Cannot make database dump because of databse is empty');
            exit();
        }

        $this->comment('Getting data from databse...');

        foreach ($tables as $table) {
            $result = DB::select('SELECT * FROM ' . $table->$colname);
            for ($i = 0; $i < count($result); $i++) {
                $result[$i] = (array) $result[$i];
                foreach (array_keys($result[$i]) as $key) {
                    $backup[$table->$colname][$i][$key] = $result[$i][$key];
                }
            }
        }

        unset($backup['migrations']);

        $this->comment('Writing data to file...');

        $f = fopen('./././database/dump/dump', 'w+');

        fwrite($f, serialize($backup));
        fclose($f);

        $this->comment('Database dump is done');
    }
}
