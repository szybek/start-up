<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;

class DatabaseRebuild extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:rebuild';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rebuild all database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* old
         * $this->call('database:droptables');
         * $this->callSilent('migrate');
         * $this->comment("Database was rebuild");
         */

        //**********************
        //setting base variables
        //column which is in result of query "show tables"
        $colname = 'Tables_in_' . env('DB_DATABASE');
        $tables  = DB::select('SHOW TABLES');
        //backup is array with all datas in database
        $backup;
        //**********************

        //**********************
        //if in database aren't any tables making migrate
        if (empty($tables)) {
            $this->comment("Your database is empty");
            $this->comment("Making only migration");
            $this->callSilent("migrate");
            if (empty(DB::select('SHOW TABLES'))) {
                $this->comment("Migration done with fail");
            } else {
                $this->comment("Migration done with success");
            }
            exit();
        }
        //**********************

        //**********************
        //making backup and getting tables config
        foreach ($tables as $table) {
            $result = DB::select('SELECT * FROM ' . $table->$colname);
            for ($i = 0; $i < count($result); $i++) {
                $result[$i] = (array) $result[$i];
                foreach (array_keys($result[$i]) as $key) {
                    $backup[$table->$colname][$i][$key] = $result[$i][$key];
                }
            }

            $current = DB::select("describe " . $table->$colname);
            foreach ($current as $fields) {
                $old_config[$table->$colname][] = ((array) $fields)['Field'];
            }

            DB::select("RENAME TABLE " . $table->$colname . " to backup" . $table->$colname);
        }
        //**********************

        // print_r($backup);
        // print_r($old_config);

        $this->callSilent("migrate");
        unset($backup['migrations']);

        //*************************
        //check integrity datas

        foreach ($tables as $table) {
            $current = DB::select("describe " . $table->$colname);
            foreach ($current as $fields) {
                $new_config[$table->$colname][] = ((array) $fields)['Field'];
            }
        }

        if ($old_config !== $new_config) {
            $this->comment("Datas are not integrated" . PHP_EOL . "Let's integrate it");
            $difference = $this->getDifference($old_config, $new_config);
            if (!empty($difference['del'])) {
                $this->comment("Deleting datas...");
                foreach ($difference['del'] as $deleleTable => $deleleTableConfig ) {
                    //$deleleTable => string => table name where datas were deleted
                    //$deleleTableConfig => array => names of fields
                    if (isset($backup[$deleleTable]) && !empty($backup[$deleleTable])) {
                        foreach ($deleleTableConfig as $field) {
                            foreach ($backup[$deleleTable] as $key => $row) {
                                unset($backup[$deleleTable][$key][$field]);
                            }
                        }
                    }
                }
            }

            if (!empty($difference['add'])) {
                $this->comment("Adding datas...");
                foreach ($difference['add'] as $addTable => $addTableConfig ) {
                    //$addTable => string => table name where datas were added
                    //$addTableConfig => array => names of fields
                    if (isset($backup[$addTable]) && !empty($backup[$addTable])) {
                        foreach ($addTableConfig as $field) {
                            $add = $this->ask('Enter data to fill table ' . $addTable . ' on field ' . $field);
                            foreach ($backup[$addTable] as $key => $row) {
                                $backup[$addTable][$key][$field] = $add;
                            }
                        }
                    }
                }
            }

            foreach ($tables as $table) {
                DB::select("DROP TABLE IF EXISTS backup" . $table->$colname);
            }
        }

        //end check integrity datas
        //*************************

        $this->call("database:droptables");
        $this->callSilent("migrate");

        // print_r($backup);

        foreach ($backup as $table => $datas) {
            DB::table($table)->insert($datas);
        }

        $this->comment("Database rebuild is done");
    }

    private function getDifference($old_config, $new_config) 
    {
        $res = array(
            'del' => array(),
            'add' => array()
        );

        foreach ($old_config as $table => $config) {
            if (array_key_exists($table, $new_config)) {
                foreach ($config as $value) {
                    if (!in_array($value, $new_config[$table])) {
                        $res['del'][$table][] = $value;
                    }
                }
            } else {
                $res['del'][$table] = $config;
            }
        }

        foreach ($new_config as $table => $config) {
            foreach ($config as $value) {
                if (!in_array($value, $old_config[$table])) {
                    $res['add'][$table][] = $value;
                }
            }
        }

        return $res;
    }
}