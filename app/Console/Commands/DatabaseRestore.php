<?php

namespace App\Console\Commands;

use DB;

use Illuminate\Console\Command;

class DatabaseRestore extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:restore';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $filename = './././database/dump/dump';
        if (!file_exists($filename)) {
            $this->comment('Database dump doesn\'t exist, making only migration');
            $this->call('migrate');
            exit();
        }

        $this->comment('Making migration');
        $this->call('migrate');

        $this->comment('Getting data form databse dump');
        $f = fopen($filename, 'r+');
        $dump = unserialize(fread($f, filesize($filename)));

        fclose($f);

        $this->comment('Inserting data into database');
        foreach ($dump as $table => $rows) {
            DB::table($table)->insert($rows);
        }

        $this->comment('Restoring database is done');
    }
}
