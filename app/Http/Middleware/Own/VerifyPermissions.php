<?php

namespace App\Http\Middleware\Own;

use DB;
use Closure;

use App\Models\Permission;

use Illuminate\Support\Facades\Auth;

class VerifyPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check() || 
            !(bool)Permission::where('name', 'admin.panel.view')
                ->first()
                ->permissions()
                ->where('code', Auth::user()->type)
                ->value('value')) {
            abort(404);
        }

        $path = $request->url();

        $menu = $this->getMenu($path, $request, [
            'dashboard'  => ['index'],
            'user'       => ['index', 'show', 'edit'],
            'permission' => ['index', 'show', 'edit']
        ]);

        $permissions['user']       = $this->getPermissions('user', ['view', 'modify', 'destroy', 'permissions']);
        $permissions['permission'] = $this->getPermissions('permission', ['view', 'modify']);

        if ($this->checkRoute($path, 'user',       ['view', 'modify', 'destroy'], $permissions, $request) ||
            $this->checkRoute($path, 'permission', ['view', 'modify'],            $permissions, $request)) {
            return response(view('admin.access_denied')->with(['permissions' => $permissions, 'menu' => $menu]));
        }

        view()->composer('*', function ($view) use ($permissions, $menu) {
            $view->with(['permissions' => $permissions, 'menu' => $menu]);
        });

        return $next($request);
    }

    /**
     * Get permissions 
     */
    private function getPermissions($which, $perm)
    {
        foreach ($perm as $value) {
            $permissions[$value] = Permission::where('name', 'admin.'. $which .'.'. $value)
                ->first()
                ->permissions()
                ->where('code', Auth::user()->type)
                ->value('value');
        }

        return $permissions;
    }

    /**
     * Return true if user have not permission
     */
    private function checkRoute($path, $which, $check, $permissions, $request)
    {
        if (in_array('view', $check)) {
            $index   = route('admin.'. $which .'.index');
            $show    = route('admin.'. $which .'.show', $request->route($which));

            if (($path ==  $index || $path == $show) && $permissions[$which]['view'] != 1) {
                return true;
            }
        }

        if (array_search('modify', $check)) {
            $edit    = route('admin.'. $which .'.edit', $request->route($which));
            $update  = route('admin.'. $which .'.update', $request->route($which));

            if ((($path == $update && $request->isMethod('put')) || $path == $edit) && 
                ($permissions[$which]['modify'] != 1 || $permissions[$which]['view'] != 1)) {
                return true;
            }
        }

        if (array_search('destroy', $check)) {
            $destroy = route('admin.'. $which .'.destroy', $request->route($which));
            if ($path == $destroy && $request->isMethod('delete') && $permissions[$which]['destroy'] != 1) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get current menu element
     */
    public function getMenu($path, $request, $arr)
    {
        foreach ($arr as $group => $check) {
            foreach ($check as $element) {
                $route = 'admin.' . $group . '.' . $element;
                if (($element == "index" && $path == route($route)) ||
                    $path == route($route, $request->route($group))) {
                    return $group;
                }
            }
        }
        return "";
    }
}