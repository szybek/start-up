<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers; 

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        $accepted = $user->accepted;

        if ( $accepted == 1) {
            return false;
        } else {
            Auth::logout();
            $request->session()->regenerate();
            $request->session()->put('administrative_error', true);
            return redirect()->route('profile.login');
        }
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return ($auth = $this->authenticated($request, $this->guard()->user()))
                ? $auth : redirect()->intended($this->redirectPath());
    }

    protected function validateLogin(Request $request)
    {
        $this->validate(
            $request, 
            [
                $this->username() => 'required|string',
                'password' => 'required|string',
            ],
            [
                'required' => 'To pole jest wymagane'
            ]);
    }

    public function username()
    {
        return 'name';
    }
}
