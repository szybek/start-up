<?php

namespace App\Http\Controllers\Admin;

use App\Models\UserName;
use App\Models\Permission;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userGroup = UserName::all();
        return view('admin.permission.index')
            ->with('userGroup', $userGroup);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($group)
    {
        $permissions = $this->getPermissions($group);

        return view('admin.permission.show')
            ->with('userPermissions', $permissions);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($group)
    {
        $permissions = $this->getPermissions($group);

        return view('admin.permission.edit')
            ->with('userPermissions', $permissions); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $group)
    {
        foreach ($request->perm as $key => $value) {
            try {
                UserName::where('code', $group)
                    ->first()
                    ->permissions()
                    ->attach($key, ['value' => $value]);
            } catch (\Illuminate\Database\QueryException $e) {
                UserName::where('code', $group)
                    ->first()
                    ->permissions()
                    ->updateExistingPivot($key, ['value' => $value]);
            }
        }

        return redirect(route('admin.permission.show', $group));
    }

    /**
     * Get merged permissions
     */
    private function getPermissions($group  )
    {
        $permissions = UserName::where('code', $group)
            ->first()
            ->permissions()
            ->get();
        $permissionsName = Permission::all();
        foreach ($permissionsName as $permissionName) {
            foreach ($permissions as $permission) {
                if ($permission->pivot->id_permission_name == $permissionName->id) {
                    $ids[] = $permission->pivot->id_permission_name;
                    $res[] = [
                        'id'        => $permission->pivot->id_permission_name,
                        'name'      => $permission->name,
                        'describe'  => $permission->describe,
                        'value'     => $permission->pivot->value
                    ];
                }
            }
        }
        if (!isset($ids)) {
            $ids = [];
        }
        foreach ($permissionsName as $permissionName) {
            if (!in_array($permissionName->id, $ids)) {
                $res[] = [
                    'id'       => $permissionName->id,
                    'name'     => $permissionName->name,
                    'describe' => $permissionName->describe,
                    'value'    => "0"
                ];
            }
        }

        usort($res, function($a, $b) {
            return $a['id'] <=> $b['id'];
        });

        return $res;
    }
}
