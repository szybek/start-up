<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Models\UserDetail;
use App\Models\CompanyDetail;
use App\Models\UserName;
use App\Models\Permission;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        foreach ($users as $user) {
            $user->type = UserName::where('code', $user->type)->value('name');
        }

        return view('admin.user.index')
            ->with('users', $users);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        if (empty($user)) {
            return view('admin.user.dont_exists');
        }

        $details = $this->getDetails($user);
        $user->type = UserName::where('code', $user->type)->value('name');

        return view('admin.user.show')
            ->with('user', $user)
            ->with('details', $details);        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $usersName = UserName::all();

        $details = $this->getDetails($user);
        $user->type = UserName::where('code', $user->type)->value('name');

        return view('admin.user.edit')
            ->with('user', $user)
            ->with('usersName', $usersName)
            ->with('details', $details);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' =>     'required|string|max:255',
            'email' =>    'required|string|email|max:255',
            'active' =>   [ Rule::in(['0', '1']) ],
            'accepted' => [ Rule::in(['0', '1']) ],
        ]);

        if ($validator->fails()) {
            return redirect(route('admin.user.edit', $id))
                    ->withErrors($validator);
        } else {
            $user = User::find($id);

            $user->name = $request->name;
            $user->email = $request->email;
            $user->active = $request->active;
            $user->accepted = $request->accepted;

            if ($user->type == UserName::where('name', 'Firma')->value('code')) {
                $model = $user->companyDetails;
            } else {
                $model = $user->userDetails;
            }

            foreach ($request->all()['details'] as $field => $value) {
                $model->$field = $value;
            }

            if (UserName::where('code', Auth::user()->type)
                ->first()
                ->permissions()
                ->where('name', 'admin.user.permissions')
                ->value('value') == 1) {
                $user->type = $request->type;
            }

            $user->save();
            $model->save();

            return redirect(route('admin.user.show', $id));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return redirect(route('admin.user.index'));
    }

    private function getDetails($user)
    {
        $id = $user->id;
        if ($user->type == UserName::where('name', 'Firma')->value('code')) {
            $details = $user->companyDetails;
            $detail[] = ['name' => 'Nazwa',        'value' => $details->name   ,   'field' => 'name'];
            $detail[] = ['name' => 'Adres',        'value' => $details->address,   'field' => 'address'];
            $detail[] = ['name' => 'Kod pocztowy', 'value' => $details->post_code, 'field' => 'post_code'];
            $detail[] = ['name' => 'Miasto',       'value' => $details->city,      'field' => 'city'];
        } else {
            $details = $user->userDetails;
            $detail[] = ['name' => 'Imię',           'value' => $details->firstname,  'field' => 'firstname'];
            $detail[] = ['name' => 'Nazwisko',       'value' => $details->lastname,   'field' => 'lastname'];
            $detail[] = ['name' => 'Data urodzenia', 'value' => $details->birth_date, 'field' => 'birth_date'];
        }

        return $detail;
    }
}
