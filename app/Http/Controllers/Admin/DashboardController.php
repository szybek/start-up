<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Models\UserName;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function showDashboard()
    {
    	return view('admin.dashboard')
    		->with('user_count', User::count())
    		->with('group_count', UserName::count())
    		->with('companies_count', User::where('type', 
    			UserName::where('name', 'Firma')
    			->value('code'))->count());
    }
}
