<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type', 'active', 'accepted'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($value) 
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Get details of company
     */
    public function companyDetails()
    {
        return $this->hasOne('App\Models\CompanyDetail', 'id_user');
    }

    /**
     * Get details of user
     */
    public function userDetails()
    {
        return $this->hasOne('App\Models\UserDetail', 'id_user');
    }

    /**
     * Get places of user
     */
    public function places()
    {
        return $this->hasMany('App\Models\Place', 'id_user');
    }
}
