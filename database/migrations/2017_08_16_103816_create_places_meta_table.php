<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places_meta', function (Blueprint $table) {
            $table->integer('id_place')->unsigned();
            $table->integer('id_meta_name')->unsigned();;
            $table->string('value');
            $table->primary(['id_place', 'id_meta_name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places_meta');
    }
}
