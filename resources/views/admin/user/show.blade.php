@extends('layouts.admin')

@section('content')
	@if (!empty($user))
		<table class="table">
			<tr>
				<td class="text-right" style="border-top:0;">
					<strong>Name: </strong>
				</td>
				<td style="border-top:0;">{{ $user->name }}</td>
			</tr>
			<tr>
				<td class="text-right">
					<strong>Email: </strong>
				</td>
				<td>{{ $user->email }}</td>
			</tr>
			<tr>
				<td class="text-right">
					<strong>Utworzony: </strong>
				</td>
				<td>{{ $user->created_at }}</td>
			</tr>
			<tr>
				<td class="text-right">
					<strong>Modyfikowany: </strong>
				</td>
				<td>{{ $user->updated_at }}</td>
			</tr>
			<tr>
				<td class="text-right">
					<strong>Typ: </strong>
				</td>
				<td>{{ $user->type }}</td>
			</tr>
			<tr>
				<td class="text-right">
					<strong>Aktywny: </strong>
				</td>
				<td>@if ($user->active == 0) Nie @else Tak @endif</td>
			</tr>
			<tr>
				<td class="text-right">
					<strong>Zaakceptowany: </strong>
				</td>
				<td>@if ($user->accepted == 0) Nie @else Tak @endif</td>
			</tr>
			@foreach ($details as $detail)
				<tr>
					<td class="text-right">
						<strong>{{ $detail['name'] }}: </strong>
					</td>
					<td>{{ $detail['value'] }}</td>
				</tr>
			@endforeach
			@if ($permissions['user']['modify'] == 1 || $permissions['user']['destroy'] == 1)
				<tr>
					@if ($permissions['user']['modify'] == 1)
						<td class="text-right">
							<a href="{{ route('admin.user.edit', $user->id) }}" class="btn btn-warning">Modyfikuj</a>
						</td>
						@if ($permissions['user']['destroy'] == 0)
							<td></td>
						@endif
					@endif
					@if ($permissions['user']['destroy'] == 1)
						@if ($permissions['user']['modify'] == 0)
							<td></td>
						@endif
						<td>
							<form method="post" action="{{ route('admin.user.destroy', $user->id) }}">
								{{ csrf_field() }}
								{{ method_field('DELETE') }}
								<button class="btn btn-danger">Usuń</button>
							</form>
						</td>
					@endif
				</tr>
			@endif
		</table>
	@else
		<h2>Dany użytkownik nie istnieje</h2>
	@endif
@endsection