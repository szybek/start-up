@extends('layouts.admin')

@section('content')
	<h2>Wszyscy użytkownicy</h2>
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Name</th>
					<th>Email</th>
					<th>Data utworzenia</th>
					<th>Typ</th>
					<th>Aktywny</th>
					<th>Zaakceptowany</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $user)
					<tr>
						<td><a href="{{ route('admin.user.show', $user->id) }}">{{ $user->name }}</a></td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->created_at }}</td>
						<td>{{ $user->type }}</td>
						<td>@if ($user->active == 0) Nie @else Tak @endif</td>
						<td>@if ($user->accepted == 0) Nie @else Tak @endif</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection