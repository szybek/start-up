@extends('layouts.admin')

@section('content')
	@if (!empty($user))
		<form method="post" action="{{ route('admin.user.update', $user->id) }}">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			<table class="table">
				<tr>
					<td class="text-right" style="border-top:0;">
						<strong>Name: </strong>
					</td>
					<td style="border-top:0;">
						<input type="text" name="name" class="form-control" value="{{ $user->name }}">
					</td>
				</tr>
				<tr>
					<td class="text-right">
						<strong>Email: </strong>
					</td>
					<td>
						<input type="text" name="email" class="form-control" value="{{ $user->email }}">
					</td>
				</tr>
				<tr>
					<td class="text-right">
						<strong>Utworzony: </strong>
					</td>
					<td>{{ $user->created_at }}</td>
				</tr>
				<tr>
					<td class="text-right">
						<strong>Modyfikowany: </strong>
					</td>
					<td>{{ $user->updated_at }}</td>
				</tr>
				<tr>
					<td class="text-right">
						<strong>Typ: </strong>
					</td>
					<td>
						@if ($permissions['user']['permissions'] == 1)
							<select class="form-control" name="type">
								@foreach($usersName as $userName)
									<option value="{{ $userName->code }}" @if($userName->name == $user->type) selected @endif>
										{{ $userName->name }}
									</option>
								@endforeach
							</select>
						@else
							{{ $user->type }}
						@endif
					</td>
				</tr>
				<tr>
					<td class="text-right">
						<strong>Aktywny: </strong>
					</td>
					<td>
						<select class="form-control" name="active">
							<option value="0" @if ($user->active == 0) selected @endif>Nie</option>
							<option value="1" @if ($user->active == 1) selected @endif>Tak</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="text-right">
						<strong>Zaakceptowany: </strong>
					</td>
					<td>
						<select class="form-control" name="accepted">
							<option value="0" @if ($user->accepted == 0) selected @endif>Nie</option>
							<option value="1" @if ($user->accepted == 1) selected @endif>Tak</option>
						</select>
					</td>
				</tr>
				@foreach ($details as $detail)
				<tr>
					<td class="text-right">
						<strong>{{ $detail['name'] }}: </strong>
					</td>
					<td><input class="form-control" type="text" name="details[{{ $detail['field'] }}]" value="{{ $detail['value'] }}"></td>
				</tr>
				@endforeach
				<tr>
					<td class="text-center" colspan="2">
						<button class="btn btn-primary">Modyfikuj</button>
					</td>
				</tr>
			</table>
		</form>
	@else
		<h2>Dany użytkownik nie istnieje</h2>
	@endif
@endsection