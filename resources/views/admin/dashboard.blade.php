@extends('layouts.admin')

@section('title')
	Panel administracyjny
@endsection

@section('content')
	<div class="row">
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-users fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge">{{ $user_count }}</div>
							<div>Użytkowników!</div>
						</div>
					</div>
				</div>
				<a href="{{ route('admin.user.index') }}">
					<div class="panel-footer">
						<span class="pull-left">Zobacz ich</span>
						<span class="pull-right">
							<i class="fa fa-arrow-circle-right"></i>
						</span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-green">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-users fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge">{{ $group_count }}</div>
							<div>Grup!</div>
						</div>
					</div>
				</div>
				<a>
					<div class="panel-footer">
						<span class="pull-left"></span>
						<span class="pull-right">&nbsp;
						</span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-yellow">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-users fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge">{{ $companies_count }}</div>
							<div>Firm!</div>
						</div>
					</div>
				</div>
				<a>
					<div class="panel-footer">
						<span class="pull-left"></span>
						<span class="pull-right">&nbsp;
						</span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
	</div>
@endsection