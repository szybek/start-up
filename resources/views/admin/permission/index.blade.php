@extends('layouts.admin')

@section('content')
	<h2>Uprawnienia</h2>
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Nazwa group użytkowników</th>
				</tr>
			</thead>
			<tbody>
				@foreach($userGroup as $group)
					<tr>
						<td><a href="{{ route('admin.permission.show', $group->code) }}">{{ $group->name }}</a></td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection