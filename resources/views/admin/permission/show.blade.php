@extends('layouts.admin')

@section('content')
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Uprawnienie</th>
				<th>Wartość</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($userPermissions as $userPermission)
				<tr>
					<td>{{ $userPermission['describe'] }}</td>
					<td>@if($userPermission['value'] != 1) Nie @else <strong>Tak</strong> @endif</td>
				</tr>
			@endforeach
			@if ($permissions['permission']['modify'] == 1)
				<tr>
					@if ($permissions['permission']['modify'] == 1)
						<td>
							<a href="{{ route('admin.permission.edit', Request::route('permission')) }}" class="btn btn-warning">Modyfikuj</a>
						</td>
						<td></td>
					@endif
				</tr>
			@endif
		</tbody>
	</table>
@endsection