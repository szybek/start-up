@extends('layouts.admin')

@section('content')
	<form action="{{ route('admin.permission.update', Request::route('permission')) }}" method="POST">
		{{ csrf_field() }}
		{{ method_field('PUT') }}
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Uprawnienie</th>
					<th>Wartość</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($userPermissions as $userPermission)
					<tr>
						<td>
							@if ($userPermission['value'] == 1)
								<strong>{{ $userPermission['describe'] }}</strong>
							@else
								{{ $userPermission['describe'] }}
							@endif
						</td>
						<td>
							<select name="perm[{{ $userPermission['id'] }}]" class="form-control">
								<option value="0" @if($userPermission['value'] != 1) selected @endif>Nie</option>
								<option value="1" @if($userPermission['value'] == 1) selected @endif>Tak</option>
							</select>
						</td>
					</tr>
				@endforeach
				<tr>
					<td>
						<button class="btn btn-warning">Modyfikuj</button>
					</td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</form>
@endsection